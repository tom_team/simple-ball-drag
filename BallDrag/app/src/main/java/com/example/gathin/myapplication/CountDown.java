package com.example.gathin.myapplication;

import android.os.CountDownTimer;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class CountDown extends CountDownTimer {
    private TextView textViewTimer;
    private long mMillisInFuture;

    public CountDown(long millisInFuture, long countDownInterval,TextView tv) {
        super(millisInFuture, countDownInterval);
        mMillisInFuture = millisInFuture;
        textViewTimer = tv;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        long ms = (mMillisInFuture - millisUntilFinished);
        //String text = String.valueOf((int)ms);
        String text = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)),
                TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms)));
        textViewTimer.setText(text);
    }

    @Override
    public void onFinish() {
        textViewTimer.setText("Time up");
    }
}