package com.example.gathin.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    private int _xDelta;
    private int _yDelta;
    private ViewGroup mRrootLayout;

    private ViewGroup.LayoutParams layoutParams ;
    private TextView mTv;
    private ImageView mBalls;
    private ImageView mBalls2;
    private final static long ONE_SECOND = 1000;
    CountDown mTimer;
    private AlertDialog mDialog = null;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTv = (TextView)findViewById(R.id.time);
        mBalls = (ImageView)findViewById(R.id.basketball1);
        mBalls2 = (ImageView)findViewById(R.id.basketball2);
        //RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(200,200);
        //RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(200,200);
        //mBalls.setLayoutParams(layoutParams);
       // mBalls2.setLayoutParams(layoutParams2);
        mDialog = new AlertDialog.Builder(MainActivity.this, AlertDialog.THEME_HOLO_LIGHT)
                .setTitle("play again")
                .setMessage("You can play it again")
                .setIcon(R.id.basketball1)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                mTimer.start();
                                mBalls.setY(System.currentTimeMillis() % 1000);
                                mBalls2.setY(System.currentTimeMillis() % 1000 + 400);

                            }
                        }
                )
                .create();
        mDialog.setCanceledOnTouchOutside(false);

        TouchListener touchListener = new TouchListener();
        mBalls.setOnTouchListener(touchListener);
        mBalls2.setOnTouchListener(touchListener);

        mTimer = new CountDown(60 * 60 * ONE_SECOND, ONE_SECOND, mTv);
        mTimer.start();
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    checkTime();
                }
            }


        }).start();
    }

    private void checkTime() {
        //mHandler.sendEmptyMessage(0);
        waitOneSecond();
        int y1 = (int) mBalls.getY();
        int y2 = (int) mBalls2.getY();
        int dist = (y1 - y2) < 0 ? y2 - y1 : y1 - y2 ;

        Log.d("TOM", "=========== mBalls.getBottom() = " +   y1 );
        Log.d("TOM", "=========== mBalls2.Top() = " +  y2);
        if ( dist < 20 ) {
            mHandler.sendEmptyMessage(0);
            mTimer.cancel();
        }
    }

    public static void waitOneSecond() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    Handler mHandler = new Handler() {
        public void handleMessage(Message pMsg) {
            super.handleMessage(pMsg);
            mTv.setText("Finshed !!");
            mDialog.show();
        }
    };

    class TouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    _xDelta = X - lParams.leftMargin;
                    _yDelta = Y - lParams.topMargin;
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                            .getLayoutParams();
                    layoutParams.leftMargin = X - _xDelta;
                    layoutParams.topMargin = Y - _yDelta;
                    layoutParams.rightMargin = -250;
                    layoutParams.bottomMargin = -250;
                    view.setLayoutParams(layoutParams);
                    break;
            }
            //mRrootLayout.invalidate();
            return true;
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
